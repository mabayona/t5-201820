package view;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.data_structures.MaxHeapCP;
import model.data_structures.PriorityQueue;
import model.vo.VOBike;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		VOTrip[] nTrips = null;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				Controller.loadTrips();

				System.out.println("Tama�o de la pila de viajes: " + Controller.TripsSSize());
				System.out.println("Tama�o de la pila de estaciones: " + Controller.TripsSSize());


				break;

			case 2:
				// Requerimiento 2
				System.out.println("Ingrese en n�mero de bicicletas a agregar (MAXHEAP):");
				int numberOfBikes = Integer.parseInt(sc.next());

				System.out.println("- Muestra de " + numberOfBikes + " bicicletas- ");

				Stopwatch timerX=new Stopwatch();

				MaxHeapCP<VOBike> bikesxxx =Controller.CrearMonticulo(numberOfBikes);

				double timeX= timerX.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura agregando una muestra de " +numberOfBikes+""+ timeX);


				Stopwatch timerY=new Stopwatch();

				VOBike maximo=bikesxxx.max();

				double timeY= timerY.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura sacando el maximo (ID:"+maximo.darId()+") de una muestra de " + numberOfBikes +""+ timeY);

				break;

			case 3:
				System.out.println("Ingrese en n�mero de bicicletas a agregar (PRIORITY QUEUE):");
				int numberOfBikes2 = Integer.parseInt(sc.next());

				System.out.println("- Muestra de " + numberOfBikes2 + " bicicletas- ");

				Stopwatch timerX2=new Stopwatch();

				PriorityQueue<VOBike> bikesxxx2 =Controller.CrearCola(numberOfBikes2);

				double timeX2= timerX2.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura agregando una muestra de " +numberOfBikes2+": "+ timeX2);


				Stopwatch timerY2=new Stopwatch();

				VOBike maximo2=bikesxxx2.max();

				double timeY2= timerY2.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura sacando el maximo (ID:"+maximo2.darId()+") de una muestra de " + numberOfBikes2 +": "+ timeY2);

				break;


			case 4:
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1A = sc.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq1A = sc.next();

				// Datos Fecha/Hora inicial

				LocalDateTime startDate = convertirFecha_Hora_LDT(fechaInicialReq1A, horaInicialReq1A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq1A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq1A = sc.next();

				// Datos Fecha/Hora final

				LocalDateTime finalDate = convertirFecha_Hora_LDT(fechaFinalReq1A , horaFinalReq1A);

				Stopwatch timerA=new Stopwatch();
				MaxHeapCP<VOBike> bikesDates=Controller.CrearMonticulo(startDate, finalDate);
				double timeA= timerA.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura agregando todas las bicicletas entre fechas");
				System.out.println("es de "+ timeA);

				Stopwatch timerB=new Stopwatch();
				VOBike bb=bikesDates.max();
				double timeB= timerB.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura sacando el maximo(ID:"+bb.darId()+") de todas las bicicletas entre fechas");
				System.out.println("es de "+ timeB);

			case 5:	

				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2A = sc.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2A = sc.next();

				// Datos Fecha/Hora inicial

				LocalDateTime startDate2 = convertirFecha_Hora_LDT(fechaInicialReq2A, horaInicialReq2A);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq2A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2A = sc.next();

				// Datos Fecha/Hora final

				LocalDateTime finalDate2 = convertirFecha_Hora_LDT(fechaFinalReq2A , horaFinalReq2A);

				Stopwatch timerA2=new Stopwatch();
				PriorityQueue<VOBike> bikesDates2=Controller.crearColaP(startDate2, finalDate2);
				double timeA2= timerA2.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura agregando todas las bicicletas entre fechas");
				System.out.println("es de "+ timeA2);

				Stopwatch timerB2=new Stopwatch();
				VOBike bb2=bikesDates2.max();
				double timeB2= timerB2.elapsedTime();

				System.out.println("El tiempo (en milisegundos) que dura sacando el maximo(ID:"+bb2.darId()+") de todas las bicicletas entre fechas");
				System.out.println("es de "+ timeB2);

				break;
			case 6:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1. Cargar viajes y estaciones");
		System.out.println("2. El tiempo (en milisegundos) que dura agregando una muestra N bicicletas (HEAP)");
		System.out.println("3. El tiempo (en milisegundos) que dura agregando una muestra N bicicletas (QUEUE)");
		System.out.println("4. Generar cola de prioridad de bicicletas de acuerdo con la distancia recorrida \n"+"APARTIR DE FECHAS Heap");
		System.out.println("5. Generar cola de prioridad de bicicletas de acuerdo con la distancia recorrida \n"+"APARTIR DE FECHAS Queue");
		System.out.println("6. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
}
