package api;

import java.time.LocalDateTime;

import model.data_structures.Stack;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	Stack<VOTrip> loadTrips(String tripsFile);

	VOTrip[] getNTrips(int n);

	int getTripsSSize();

}
