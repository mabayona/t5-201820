package controller;

import java.time.LocalDateTime;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.MaxHeapCP;
import model.data_structures.PriorityQueue;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	
	public static void loadTrips() {
		manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");
		manager.loadTrips("./data/Divvy_Trips_2017_Q4.csv");
		manager.loadStations("./data/Divvy_Stations_2017_Q3Q4.csv");
	}
		
	public static int TripsSSize() {
		return manager.getTripsSSize();
	}
	
	
	
	public static MaxHeapCP<VOBike> CrearMonticulo(LocalDateTime initial, LocalDateTime finalp) {
		  return manager.crearMonticuloCP(initial, finalp);
	}
	
	public static PriorityQueue<VOBike> crearColaP(LocalDateTime initial, LocalDateTime finalp) {
		  return manager.crearColaP(initial, finalp);
	}

	
	public static MaxHeapCP<VOBike> CrearMonticulo(int max) {
		 return manager.crearMonticuloCP(max);
	}
	
	public static PriorityQueue<VOBike> CrearCola(int max) {
		 return manager.crearColaP(max);
	}

	public static VOTrip[] getNTrips (int n) {
		return manager.getNTrips(n);
	}
	
	
}
