package model.vo;

/**
 * Representation of a bike object
 */
public class VOBike implements Comparable<VOBike>{

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private int id;
	private double totalDistancia;



	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	public VOBike(int id) {
		super();
		this.id = id;
		this.totalDistancia = 0;
	}

	public VOBike() {
		this.id = 0;
		this.totalDistancia = 0;

	}

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	public int darId() {
		return id;
	}
	
	public double getTotalDistancia() {
		return totalDistancia;
	}
	
	public void addDistanceToTotalDistancia(double totalDistancia) {
		this.totalDistancia += totalDistancia;
	}


	@Override
	public int compareTo(VOBike o) {
		if(totalDistancia > o.getTotalDistancia()){
			return 1;
		}else if(totalDistancia< o.getTotalDistancia()){
			return -1;
		}
		else
			return 0;
	}


}



