package model.data_structures;

public class PriorityQueue<E extends Comparable<E>> {

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private int maxSize;
	
	protected int size;

	
	protected Node<E> headNode;
	protected Node<E> lastNode;


	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	/**
	 * Crea una cola a partir del elmento que se pasa por parametro.
	 * @param t El Elemento (T extends Comparable) que llega por parametro
	 */
	public PriorityQueue(int max) {
		headNode = null;
		lastNode = null;

		maxSize = max;
	}

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------

	public int darNumElementos() {
		return size;
	}

	public int maxSize() {
		return maxSize;
	}

	public boolean isEmpty() {
		return headNode == null;
	}

	/**
	 *A�ade un elemento ordenadamente en la cola 
	 *Si los elementos son iguales a�ade el elemento nuevo despues del viejo.
	 * @return true si se pudo a�adir el elemeno, false de lo contrario 
	 */
	public void addE(E e) throws Exception{
		
		boolean added = false;
		
		if(this.size == maxSize)
			throw new Exception("No puede a�adir m�s elementos; se ha alcanzado el tope maximo");
		
		if(e!=null) {
			Node<E> nuevo = new Node<E>(e); //elemento que se va a�adir
			Node<E> actual = this.headNode; // elemento actual de la iteracion
			
			//CASO 1: La cola est� vac�a
			if(this.size == 0) {
				this.headNode = nuevo;
				this.lastNode = nuevo;
				added = true;
			}
			
			//CASO 2: La cola no est� vacia
			else {
				while(actual != null && !added) {
					int comp = actual.darElemento().compareTo(nuevo.darElemento());
					
					if(comp > 0) { // el elemento es menor, sigue iterando
						actual = actual.darSiguiente();
						if(actual == null) { //llego al ultimo y aun el nuevo es menor: lo a�ade al final
							nuevo.cambiarAnterior(lastNode);
							lastNode.cambiarSiguiente(nuevo);
							lastNode = nuevo;
							added = true;
						}
					}
				
					else if(comp < 0) { // el elemento nuevo es mayor, debe a�adir
						if(actual.darAnterior() == null) { //actual es HeadNode
							headNode = nuevo;
							headNode.cambiarSiguiente(actual);
							actual.cambiarAnterior(headNode);
							added = true;
						}
						else { //actual est� entre m�s nodos
							nuevo.cambiarSiguiente(actual);
							nuevo.cambiarAnterior(actual.darAnterior());
							
							actual.cambiarAnterior(nuevo);
							nuevo.darAnterior().cambiarSiguiente(nuevo);
							added = true;
						}
					}
					
					else { //los elemntos son iguales segun la comparacion; a�ade el nuevo despues del viejo
						if(actual.darSiguiente() == null) { //debe a�adir al final del ultimo 
							nuevo.cambiarAnterior(lastNode);
							lastNode.cambiarSiguiente(nuevo);
							lastNode = nuevo;
							added = true;
						}
						else {
							nuevo.cambiarSiguiente(actual.darSiguiente());
							nuevo.cambiarAnterior(actual);
							
							actual.darSiguiente().cambiarAnterior(nuevo);
							actual.cambiarSiguiente(nuevo);
							added = true;
						}
					}
					
				}
			}
		}
		if(added)
			this.size ++;
		
	}
	
	/**
	 * Retrona el elemento con mayor prioridad 
	 */
	public E max() {
		//en la cola de prioridad el elemento "mayor" se encuentra al incio de la lista (pos 0)
		
		E eliminado = null;
		if(size == 1) {
			eliminado = headNode.darElemento();
			headNode = null;
			lastNode = null;
			size --;
		}
		else if(size > 1){
			eliminado = headNode.darElemento();
			headNode = headNode.darSiguiente();
			headNode.cambiarAnterior(null);
			size --;
			
		}
			return eliminado;
	}








}
