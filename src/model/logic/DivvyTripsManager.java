package model.logic;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;
import utils.ComparisonCriteria;
import model.data_structures.DoublyLinkedList;
import model.data_structures.MaxHeapCP;
import model.data_structures.PriorityQueue;
import model.data_structures.Queue;
import model.data_structures.Stack;
import utils.*;;


public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	/*
	 * Administrador de consutas sobre bicicletas
	 */
	private	Bikes bikesM;


	private ManejadorBicicletas ManejadorBikes;

	/**
	 * Stack de trips
	 */
	private Stack<VOTrip> tripsS;

	/**
	 * Stack de stations
	 */
	private Stack<VOStation> stationsS;


	DoublyLinkedList<VOBike> lista88;

	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------
	public DivvyTripsManager() {
		tripsS=new Stack<VOTrip>();
		stationsS=new Stack<VOStation>();
		bikesM= new Bikes();
		ManejadorBikes=new ManejadorBicicletas();
		lista88=new DoublyLinkedList<VOBike>();
	}



	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------



	/**
	 * Da el tama;o actual del stack de viajes
	 * @return el tama;o actual del stack de viajes
	 */
	public int getTripsSSize() {
		return tripsS.size();
	}

	public Stack<VOStation> loadStations( String stationsFile) 
	{

		String csvFile = stationsFile;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try 
		{
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			while ((line = br.readLine()) != null)
			{   

				String[] datos = line.split(cvsSplitBy);

				int id;
				String name;
				String city;
				double latitude;
				double longitude;
				int dpcapacity;
				String online_date;

				try 
				{
					if(line.startsWith("\"")) 
					{

						id=Integer.parseInt(datos[0].substring(1, datos[0].length()-1));
						name=datos[1].substring(1, datos[1].length()-1);
						city=datos[2].substring(1, datos[2].length()-1);
						latitude=Double.parseDouble(datos[3].substring(1, datos[3].length()-1));
						longitude=Double.parseDouble(datos[4].substring(1, datos[4].length()-1));
						dpcapacity=Integer.parseInt(datos[5].substring(1, datos[5].length()-1));
						online_date=datos[6].substring(1, datos[6].length()-1);

						String[] horaYFecha = online_date.split(" ");
						String fecha = horaYFecha[0];
						String hora = horaYFecha[1];
						LocalDateTime date = convertirFecha_Hora_LDT(fecha, hora);

						stationsS.push(new VOStation( id, name,  city, latitude, longitude,  dpcapacity, date ));

					}else 
					{

						id=Integer.parseInt(datos[0]);
						name=datos[1];
						city=datos[2];
						latitude=Double.parseDouble(datos[3]);
						longitude=Double.parseDouble(datos[4]);
						dpcapacity=Integer.parseInt(datos[5]);
						online_date=datos[6];

						String[] horaYFecha = online_date.split(" ");
						String fecha = horaYFecha[0];
						String hora = horaYFecha[1];
						LocalDateTime date = convertirFecha_Hora_LDT(fecha, hora);

						stationsS.push(new VOStation(id, name,  city, latitude, longitude,  dpcapacity, date ));

					}

				} catch (NumberFormatException e) 
				{
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		lista88 =getBikesBetweenDates(convertirFecha_Hora_LDT("7/1/2017", "00:00:20"),convertirFecha_Hora_LDT("12/31/2017", "23:53:00"));
		return stationsS;
	}

	public Stack<VOTrip> loadTrips(String tripsFile) {



		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();

			int trip_id;
			LocalDateTime start_time;
			LocalDateTime end_time;
			int bikeid;
			int tripduration;
			int from_station_id;
			String from_station_name;
			int to_station_id;
			String to_station_name;
			String usertype;
			String gender;
			int birthyear;

			while ((line = br.readLine()) != null) { 

				String[] datos = line.split(cvsSplitBy);

				try {

					if(line.startsWith("\"")) 
					{

						trip_id=Integer.parseInt(datos[0].substring(1, datos[0].length()-1));



						String start=datos[1].substring(1, datos[1].length()-1);
						String startD=start.split("\\s+")[0];
						String startH=start.split("\\s+")[1];
						start_time=convertirFecha_Hora_LDT(startD, startH);


						String end=datos[2].substring(1, datos[2].length()-1);
						String endD=end.split("\\s+")[0];
						String endH=end.split("\\s+")[1];
						end_time=convertirFecha_Hora_LDT(endD, endH);


						bikeid=Integer.parseInt(datos[3].substring(1, datos[3].length()-1));
						tripduration=Integer.parseInt(datos[4].substring(1, datos[4].length()-1));
						from_station_id=Integer.parseInt(datos[5].substring(1, datos[5].length()-1));
						from_station_name=datos[6].substring(1, datos[6].length()-1);
						to_station_id=Integer.parseInt(datos[7].substring(1, datos[7].length()-1));
						to_station_name=datos[8].substring(1, datos[8].length()-1);
						usertype=datos[9].substring(1, datos[9].length()-1);

						if(datos[10].equals("\""+"\"") && datos[11].equals("\""+"\"")) {

							gender=" ";
							birthyear=0;

						}else if(datos[10].equals("\""+"\"")) {

							gender=" ";
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}else if(datos[11].equals("\""+"\"")) {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear=0 ;

						}else {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}

						VOTrip v=new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear);
						tripsS.push(v);

					}else 
					{


						trip_id=Integer.parseInt(datos[0]);

						String start1=datos[1];
						String startD1=start1.split("\\s+")[0];
						String startH1=start1.split("\\s+")[1];
						start_time=convertirFecha_Hora_LDT(startD1, startH1);


						String end1=datos[2];
						String endD1=end1.split("\\s+")[0];
						String endH1=end1.split("\\s+")[1];
						end_time=convertirFecha_Hora_LDT(endD1, endH1);



						bikeid=Integer.parseInt(datos[3]);
						tripduration=Integer.parseInt(datos[4]);
						from_station_id=Integer.parseInt(datos[5]);
						from_station_name=datos[6];
						to_station_id=Integer.parseInt(datos[7]);
						to_station_name=datos[8];
						usertype=datos[9];

						if(datos.length==10)
						{
							gender=" ";
							birthyear=0;

						}
						else if(datos.length==11)
						{
							gender=datos[10];
							birthyear=0;

						}
						else 
						{
							gender=datos[10];
							birthyear=Integer.parseInt(datos[11]);

						}

						VOTrip v = new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear);
						tripsS.push(v);

					}

				} catch (NumberFormatException e) {

					e.printStackTrace();

				} 
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return tripsS;
	}




	/**
	 * 
	 * Usa la Cola creada para retornar un arreglo de �n� viajes.
	 * @param n es el numero de las estaciones que se quiere.
	 * @return Una muestra de �n� viajes
	 */

	public  VOTrip[] getNTrips ( int n) {

		VOTrip[] nuevoArreglo= new VOTrip[n];

		System.out.println(tripsS.size());

		for (int i = 0; i < n; i++) {
			nuevoArreglo[i]=tripsS.pop();
		}

		return nuevoArreglo;
	}


	public Queue <VOTrip> getTripsBetweenDates(LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> tripsS) {
		Queue <VOTrip> respuesta=new Queue<VOTrip>();

		for(VOTrip tempTrip : tripsS) {
			if(tempTrip.getStart_time().isAfter(initialDateP) && tempTrip.getEnd_time().isBefore(finalDateP)) 
			{

				respuesta.enqueue(tempTrip);
			}
		}
		return respuesta;
	}


	/**
	 * REQUIREMENT 2 -Louis
	 * Mostrar las bicicletas ordenadas de mayor a menor por el n�mero de viajes realizados
	 * en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 * Incluye el identificador de la bicicleta, el total de viajes realizados y la distancia total
	 * recorrida por bicicleta.
	 * @param initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Bike Queue con todos las bicicletas que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 */
	public DoublyLinkedList<VOBike> getBikesBetweenDates (LocalDateTime initialDateP, LocalDateTime finalDateP) {

		Queue <VOTrip> tripsQueue=getTripsBetweenDates ( initialDateP,  finalDateP, tripsS);
		VOTrip[] tripsArr= new VOTrip[tripsQueue.size()];
		int i=0;
		for(VOTrip tripTemp: tripsQueue) {
			tripsArr[i]=tripTemp;
			i++;
		}
		DoublyLinkedList<VOBike> bikesDO= bikesM.getBikesListFromTrips(tripsArr, stationsS);
		VOBike[] bikesArr= new VOBike[bikesDO.getSize()];
		i=0;
		for(VOBike bikeTemp: bikesDO) {
			bikesArr[i]=bikeTemp;
			i++;
		}

		Ordenador<VOBike> o = new Ordenador<VOBike>();
		o.ordenar(SortingAlgorithms.QUICKSORT, false, bikesArr,(BikesComparatorByDistance)ComparisonCriteria.COMPARE_BIKES_BY_DISTANCE.getComparator());

		i=0;
		bikesDO=new DoublyLinkedList<VOBike>();

		for (i = 0; i < bikesArr.length; i++) {
			bikesDO.addAtEnd(bikesArr[i]);
		}

		return bikesDO;
	}


	public MaxHeapCP<VOBike>  crearMonticuloCP (LocalDateTime fInicial, LocalDateTime fFinal) {
		return ManejadorBikes.crearMonticuloCP(getBikesBetweenDates(fInicial, fFinal));

	}

	public PriorityQueue<VOBike>  crearColaP (LocalDateTime fInicial, LocalDateTime fFinal) {
		return ManejadorBikes.crearColaP(getBikesBetweenDates(fInicial, fFinal));

	}

	public MaxHeapCP<VOBike>  crearMonticuloCP ( int max) {

		return ManejadorBikes.crearMonticuloCP(lista88, max);

	}
	
	public PriorityQueue<VOBike>  crearColaP ( int max) {

		return ManejadorBikes.crearColaP(lista88, max);

	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);

		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 00;

		if(datosHora.length ==3) {
			segundos = Integer.parseInt(datosHora[2]);
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}










}
