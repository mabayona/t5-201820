package model.logic;

import java.time.LocalDateTime;
import java.util.Iterator;

import model.data_structures.PriorityQueue;
import model.data_structures.DoublyLinkedList;
import model.data_structures.MaxHeapCP;
import model.vo.VOBike;

public class ManejadorBicicletas {
/**
 * Dada una fecha de inicio y una fecha 
 * de finalización se construye una cola de prioridad (MAX-oriented) con las bicicletas que
 * estuvieron en servicio en ese periodo de tiempo.
 */
	
	public PriorityQueue <VOBike> crearColaP (LocalDateTime fInicial, LocalDateTime fFinal){
		
		return null;
		
	}
	public ManejadorBicicletas() {
	
}
	public MaxHeapCP<VOBike> crearMonticuloCP (DoublyLinkedList<VOBike> bikesList){
		
		MaxHeapCP<VOBike> maxHeap=new MaxHeapCP<VOBike>(bikesList.getSize());
		
		for(VOBike bike: bikesList){
			try {
				maxHeap.agregar(bike);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return maxHeap;
		
	}
	
	public PriorityQueue<VOBike> crearColaP (DoublyLinkedList<VOBike> bikesList){
		
		PriorityQueue<VOBike> cola =new PriorityQueue<VOBike>(bikesList.getSize());
		
		for(VOBike bike: bikesList){
			try {
				cola.addE(bike);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return cola;
		
	}
	
	
 public MaxHeapCP<VOBike> crearMonticuloCP (DoublyLinkedList<VOBike> bikesList, int max){
		
		MaxHeapCP<VOBike> maxHeap=new MaxHeapCP<VOBike>(max);
		
		
		Iterator<VOBike> iterator = bikesList.iterator();
		int cuenta=0;
		while(iterator.hasNext() && cuenta<max) {
			try {
				maxHeap.agregar(iterator.next());
			} catch (Exception e) {
				e.printStackTrace();
			}
		    cuenta++;
		}
		
		return maxHeap;
		
	}
 
 public PriorityQueue<VOBike> crearColaP (DoublyLinkedList<VOBike> bikesList, int max){
		
	 PriorityQueue<VOBike>  cola=new PriorityQueue<VOBike>(max);
		
		
		Iterator<VOBike> iterator = bikesList.iterator();
		int cuenta=0;
		while(iterator.hasNext() && cuenta<max) {
			try {
				cola.addE(iterator.next());
			} catch (Exception e) {
				e.printStackTrace();
			}
		    cuenta++;
		}
		
		return cola;
		
	}

}
