package utils;

import java.util.Comparator;

import model.vo.VOTrip;

public class TripsComparatorByStartDate implements Comparator<VOTrip>{


	@Override
	public int compare(VOTrip o1, VOTrip o2) {
		int comparacion = o1.getStart_time().compareTo(o2.getStart_time());
		
		if(comparacion > 0)
			return 1;
		else if(comparacion < 0)
			return -1;
		else
			return 0;
	}

}
